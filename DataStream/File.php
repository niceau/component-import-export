<?php
/**
 * Created by Pindrop.
 * User: s3nt1nel
 * Date: 23/6/17
 */

namespace Pindrop\ImportExportBundle\DataStream;


abstract class File
{
    /**
     * @var resource
     */
    protected $fileHandle;

    /**
     * @var string
     */
    protected $filePath;

    public function __construct($file)
    {
        $this->filePath = $file;
        $this->open();
    }

    abstract protected function open();

    /**
     * @return bool
     */
    abstract public function isAccessible();

    public function __destruct()
    {
        fclose($this->fileHandle);
    }

    public function getType()
    {
        return 'file';
    }

    public function isReady()
    {
        return !is_null($this->fileHandle);
    }

    public function getFilePath()
    {
        return realpath($this->filePath);
    }

    public function getFileName()
    {
        return (new \SplFileInfo($this->filePath))->getBasename();
    }
}