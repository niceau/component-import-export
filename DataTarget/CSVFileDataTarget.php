<?php
/**
 * Created by Pindrop.
 * User: s3nt1nel
 * Date: 23/6/17
 */

namespace Pindrop\ImportExportBundle\DataTarget;


class CSVFileDataTarget extends FileDataTarget
{
    /**
     * @var bool
     */
    protected $hasHeaders = true;

    /**
     * @var string
     */
    protected $delimiter = ',';

    public function write($data)
    {
        foreach ($data as $line) {
            fputcsv($this->fileHandle, $line, $this->delimiter);
        }
    }

    public function setHeaders(array $headers)
    {
        if (empty($headers)) {
            return;
        }

        rewind($this->fileHandle);
        fputcsv($this->fileHandle, $headers, $this->delimiter);
    }

    public function setHasHeaders($flag)
    {
        $this->hasHeaders = $flag;
    }

    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }
}