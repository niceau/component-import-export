<?php

namespace Pindrop\ImportExportBundle\DataTarget;

class XMLFileDataTarget extends FileDataTarget
{
    public function write($data)
    {
        if ($data instanceof \DOMDocument) {
            $data = $data->saveXML();
        }

        fwrite($this->fileHandle, $data);
    }
}
