<?php
/**
 * Created by Pindrop.
 * User: s3nt1nel
 * Date: 23/6/17
 */

namespace Pindrop\ImportExportBundle\DataTarget;


use Pindrop\ImportExportBundle\DataStream\File;

abstract class FileDataTarget extends File implements DataTarget
{
    public function isAccessible()
    {
        return true;
    }
    protected function open()
    {
        $this->fileHandle = fopen($this->filePath, 'w');
    }
}