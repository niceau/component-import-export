<?php

namespace Pindrop\ImportExportBundle\DataTarget;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class XLSXFileTarget implements DataTarget
{
    /**
     * @var string
     */
    private $filename;

    /**
     * XLSFileTarget constructor.
     *
     * @param $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'xlsx';
    }

    /**
     * @param $data
     *
     * @return mixed|void
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function write($data)
    {
        if (!$data instanceof Spreadsheet) {
            throw new \RuntimeException('File is incorrect');
        }

        $writer = new Xlsx($data);
        $writer->save($this->filename);
    }
}