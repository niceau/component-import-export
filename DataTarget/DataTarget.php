<?php
/**
 * Created by Pindrop.
 * User: s3nt1nel
 * Date: 23/6/17
 */

namespace Pindrop\ImportExportBundle\DataTarget;


interface DataTarget
{
    /**
     * @return string
     */
    public function getType();

    /**
     * @param $data
     * @return mixed
     */
    public function write($data);
}