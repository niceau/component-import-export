<?php

namespace Pindrop\ImportExportBundle\DataSource;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class XLSFileDataSource extends FileDataSource
{
    /**
     * @param IReadFilter $readFilter
     * @return Spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function read($readFilter = null)
    {
        /**  Identify the type of $inputFileName  **/
        $fileType = IOFactory::identify($this->filePath);

        /**  Create a new Reader of the type that has been identified  **/
        $reader = IOFactory::createReader($fileType);
        $reader->setReadDataOnly(true);

        if ($readFilter && $readFilter instanceof IReadFilter) {
            $reader->setReadFilter($readFilter);
        }

        $spreadsheet = $reader->load($this->filePath);

        return $spreadsheet;
    }
}
