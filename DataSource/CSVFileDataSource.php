<?php

namespace Pindrop\ImportExportBundle\DataSource;

class CSVFileDataSource extends FileDataSource
{
    /**
     * @var bool
     */
    protected $hasHeaders = true;

    /**
     * @var string
     */
    protected $delimiter = ',';

    public function read($size)
    {
        $read = 0;
        $sample = [];

        if ($this->hasHeaders) {
            fgetcsv($this->fileHandle);
        }

        while ($read < $size) {
            $data = fgetcsv($this->fileHandle, null, $this->delimiter);

            if (!$data) {
                if (!empty($sample)) {
                    yield $sample;
                } else {
                    return [];
                }
            }

            $sample[] = $data;
            $read++;
        }

        yield $sample;
    }

    public function setHasHeaders($flag)
    {
        $this->hasHeaders = $flag;
    }

    public function getHeaders()
    {
        if (!$this->hasHeaders) {
            return false;
        }

        rewind($this->fileHandle);

        return fgetcsv($this->fileHandle);
    }

    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }
}
