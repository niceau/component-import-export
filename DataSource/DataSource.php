<?php

namespace Pindrop\ImportExportBundle\DataSource;

interface DataSource
{
    /**
     * @return string
     */
    public function getType();

    /**
     * @param $size
     * @return mixed
     */
    public function read($size);
}
