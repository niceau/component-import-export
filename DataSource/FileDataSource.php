<?php

namespace Pindrop\ImportExportBundle\DataSource;

use Pindrop\ImportExportBundle\DataStream\File;

abstract class FileDataSource extends File implements DataSource
{
    public function isAccessible()
    {
        return file_exists($this->filePath) && is_readable($this->filePath);
    }

    protected function open()
    {
        if (!$this->isAccessible()) {
            throw new \RuntimeException("File '$this->filePath' is inaccessible (not readable or does not exist)");
        }

        $this->fileHandle = fopen($this->filePath, 'r');
    }
}
