<?php
/**
 * Created by Pindrop.
 * User: s3nt1nel
 * Date: 23/6/17
 */

namespace Pindrop\ImportExportBundle\Export;


use Pindrop\ImportExportBundle\DataTarget\DataTarget;

interface ExportAdapter
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param DataTarget $dataTarget
     * @return bool
     */
    public function supports(DataTarget $dataTarget);

    /**
     * @param $data
     * @param DataTarget $dataTarget
     * @return mixed
     */
    public function export($data, DataTarget $dataTarget);
}