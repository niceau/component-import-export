<?php
/**
 * Created by Pindrop.
 * User: s3nt1nel
 * Date: 16/6/17
 */

namespace Pindrop\ImportExportBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class RegisterImportAdaptersPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $engine = $container->getDefinition('import.engine');

        $adapters = $container->findTaggedServiceIds('import.adapter');

        foreach ($adapters as $id => $adapter) {
            $engine->addMethodCall('registerAdapter', [new Reference($id)]);
        }
    }
}
