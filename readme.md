Import/Export component
=======================

### Installation

1. Add library to project

Add following to **repositories** section of ```composer.json```

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "git@bitbucket.org:niceau/component-import-export.git"
    }
  ]
}
```

```bash
composer require component/import-export
```

2. Enable bundle in `AppKernel`

```php
	$bundles[] = ...
	new Pindrop\ImportExportBundle\PindropImportExportBundle(),
```


### Usage

The component is designed with idea that your domain logic which is responsible for import/export operations should belong to your domain. Given this hte bundle provides just a set of generic interfaces and two concrete classes that serve as the wrappers and callees for your logic.

1. Import

In order to import something, you need to create a class that will implement `ImportAdapter`. Example below imports report from CSV file

```php
<?php

namespace AppBundle\Import;

use Pindrop\ImportExportBundle\Import\ImportAdapter;
use Pindrop\ImportExportBundle\DataSource\DataSource;
use Pindrop\ImportExportBundle\DataSource\CSVFileDataSource;

class ImportCSVReportAdapter implements ImportAdapter
{
	public function import(DataSource $data) {...}

	public function supports(DataSource $source)
	{
		return $source instanceof CSVFileDataSource;
	}

	public function getName()
	{
		return 'import_csv_report';
	}
}

```

This adapter class should be registered in DI container with specific tags

```yaml
services:
	csv_report_import.adapter:
		class: AppBundle\Import\ImportCSVReportAdapter
		tags:
			- { name: 'import.adapter' }

```

Once container is compiled, it will be available to you through an import service:

```php
<?php

namespace AppBundle\Service;

use Pindrop\ImportExportBundle\DataSource\CSVFileDataSource;
use Pindrop\ImportExportBundle\Service\ImportEngine;

class ReportService
{
	public function __construct(ImportEngine $engine)
	{
		$this->importEngine = $engine;
	}

	public function import($pathToFile)
	{
		$source = new CSVFileDataSource($pathToFile);
		$source->setHasHeaders(false);
		$source->setDelimiter(';');

		$this->importEngine->import('import_csv_report', $source);
	}

	...
}

```

2. Export

Export works exactly the same, with only difference that tag should be `export.adapter` and you should use `ExportEngine` and `DataTarget` interface for your export target.
