<?php
/**
 * Created by Pindrop.
 * User: s3nt1nel
 * Date: 16/6/17
 */

namespace Pindrop\ImportExportBundle\Service;


use Pindrop\ImportExportBundle\DataSource\DataSource;
use Pindrop\ImportExportBundle\Import\ImportAdapter;
use Psr\Log\LoggerInterface;

class ImportEngine
{
    /**
     * @var [ImportAdapter]
     */
    protected $importAdapters;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->importAdapters = [];
        $this->logger         = $logger;
    }

    public function import($adapterName, DataSource $source)
    {
        $adapter = $this->getAdapter($adapterName);

        if (!$adapter->supports($source)) {
            throw new \RuntimeException(sprintf('Adapter %s does not support reading from %s', $adapterName, $source->getType()));
        }

        try {
            $result = $adapter->import($source);
        } catch (\Exception $e) {
            $result = null;
            $this->logger->error('Error during import: ' . $e->getMessage());
        }

        return $result;
    }

    /**
     * @param ImportAdapter $adapter
     */
    public function registerAdapter(ImportAdapter $adapter)
    {
        if (!array_key_exists($adapter->getName(), $this->importAdapters)) {
            $this->importAdapters[$adapter->getName()] = $adapter;
        }
    }

    /**
     * @param $adapterName
     * @return ImportAdapter
     */
    public function getAdapter($adapterName)
    {
        if (array_key_exists($adapterName, $this->importAdapters)) {
            return $this->importAdapters[$adapterName];
        }

        throw new \RuntimeException(sprintf('Import adapter \'%s\' not found or not configured', $adapterName));
    }
}
