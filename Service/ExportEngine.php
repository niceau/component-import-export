<?php
/**
 * Created by Pindrop.
 * User: s3nt1nel
 * Date: 23/6/17
 */

namespace Pindrop\ImportExportBundle\Service;


use Pindrop\ImportExportBundle\DataTarget\DataTarget;
use Pindrop\ImportExportBundle\DataTarget\FileDataTarget;
use Pindrop\ImportExportBundle\Export\ExportAdapter;
use Psr\Log\LoggerInterface;

class ExportEngine
{
    /**
     * @var array
     */
    protected $exportAdapters = [];

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * ExportEngine constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $adapterName
     * @param $data
     * @param DataTarget $dataTarget
     * @return mixed|null
     */
    public function export($adapterName, $data, DataTarget $dataTarget)
    {
        $adapter = $this->getAdapter($adapterName);

        if (!$adapter->supports($dataTarget)) {
            throw new \RuntimeException('Adapter %s does not support writing to %s', $adapterName, $dataTarget->getType());
        }

        try {
            $result = $adapter->export($data, $dataTarget);
        } catch (\Exception $e) {
            $result = null;
            $this->logger->error('Error during export: ' . $e->getMessage());
        }

        return $result;
    }

    /**
     * @param ExportAdapter $adapter
     */
    public function registerAdapter(ExportAdapter $adapter)
    {
        if (!array_key_exists($adapter->getName(), $this->exportAdapters)) {
            $this->exportAdapters[$adapter->getName()] = $adapter;
        }
    }

    /**
     * @param $adapterName
     * @return ExportAdapter
     */
    public function getAdapter($adapterName)
    {
        if (array_key_exists($adapterName, $this->exportAdapters)) {
            return $this->exportAdapters[$adapterName];
        }

        throw new \RuntimeException(sprintf('Export adapter \'%s\' not found or not configured', $adapterName));
    }

    /**
     * @param $filePath
     * @param $fileTypeClass
     * @return FileDataTarget
     */
    public function prepareTargetFile($filePath, $fileTypeClass)
    {
        $parts = explode('/', $filePath);
        $fileName = array_pop($parts);
        $location = join('/', $parts);

        if (!file_exists($location)) {
            mkdir($location, 0755, true);
        }

        if (!class_exists($fileTypeClass)) {
            throw new \RuntimeException(sprintf('Requested class %s does not exist', $fileTypeClass));
        }

        return new $fileTypeClass($filePath);
    }
}
