<?php

namespace Pindrop\ImportExportBundle;

use Pindrop\ImportExportBundle\DependencyInjection\Compiler\RegisterExportAdaptersPass;
use Pindrop\ImportExportBundle\DependencyInjection\Compiler\RegisterImportAdaptersPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PindropImportExportBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new RegisterImportAdaptersPass());
        $container->addCompilerPass(new RegisterExportAdaptersPass());
    }
}
