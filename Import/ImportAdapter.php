<?php
/**
 * Created by Pindrop.
 * User: s3nt1nel
 * Date: 16/6/17
 */

namespace Pindrop\ImportExportBundle\Import;


use Pindrop\ImportExportBundle\DataSource\DataSource;

interface ImportAdapter
{
    /**
     * @param DataSource $source
     * @return mixed
     */
    public function import(DataSource $source);

    /**
     * @param DataSource $source
     * @return bool
     */
    public function supports(DataSource $source);

    /**
     * @return string
     */
    public function getName();
}